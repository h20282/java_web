package com.ice.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ice.reggie.pojo.Employee;

//@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {
}
