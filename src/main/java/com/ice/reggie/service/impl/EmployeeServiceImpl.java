package com.ice.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ice.reggie.mapper.EmployeeMapper;
import com.ice.reggie.pojo.Employee;
import com.ice.reggie.service.EmployeeService;
import org.springframework.stereotype.Service;

/**
   @description 
   @author Milo
   @date 2022/1/5 10:43
*/
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper,Employee> implements EmployeeService {
}
