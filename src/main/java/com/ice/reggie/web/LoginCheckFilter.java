package com.ice.reggie.web;

import com.alibaba.fastjson.JSON;
import com.ice.reggie.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebFilter(urlPatterns = "/*")
@Slf4j
public class LoginCheckFilter implements Filter{
    public static final AntPathMatcher PATH_MACHER = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest slreq, ServletResponse slres, FilterChain filterChain) throws IOException, ServletException{
        System.out.println("------->doFilter");
        HttpServletRequest request = (HttpServletRequest) slreq;
        HttpServletResponse response = (HttpServletResponse) slres;

        String reqURI = request.getRequestURI();

        log.info("reqURI = {}", reqURI);

        String[] okUris = new String[]{
                "/employee/login",
                "/employee/logout",
                "/backend/**",
                "/front/**"
        };
        if (check(okUris, reqURI)) {
            log.info("this URI({}) is pass", reqURI);
            filterChain.doFilter(request, response);
            return ;
        }

        Object user = request.getSession().getAttribute("id");
        if (user != null) {
            log.info("user ({}) has logged in", user);
            filterChain.doFilter(request, response);
            return ;
        }

        log.info("not logged in!");
        response.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));
        return ;
    }

    private boolean check(String[] uris, String reqURI) {
        for (String uri : uris) {
            if (PATH_MACHER.match(uri, reqURI)) {
                return true;
            }
        }
        return false;
    }
}
