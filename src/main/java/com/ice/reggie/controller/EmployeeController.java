package com.ice.reggie.controller;

import lombok.extern.slf4j.Slf4j;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ice.reggie.common.R;
import com.ice.reggie.pojo.Employee;
import com.ice.reggie.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Milo
 * @description
 * @date 2022/1/5 15:22
 */
@RestController
@RequestMapping("employee")
@Slf4j
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    /**
     * 登录功能
     * 前端发送请求:
     * url:http://localhost:8080/employee/login
     * method:post
     * param: {"username":"admin","password":"123456"}
     * 前端需要是什么?
     * code:操作成功或者失败
     * msg: 操作失败后提示消息
     * data:登录成功后员工信息
     *
     * @RequestBody:将请求体中的Json数据转换为Java对象
     */

    @PostMapping("login")
    public R<Employee> login(@RequestBody Employee employee, HttpSession session) {
        System.out.println("---------->login:" + employee);

        //1.将页面传过来的密码进行加密
        String password = DigestUtils.md5DigestAsHex(employee.getPassword().getBytes());

        //构建查询条件
        LambdaQueryWrapper<Employee> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Employee::getUsername, employee.getUsername());
        wrapper.eq(Employee::getPassword, password);
        //2.根据用户名和加密后的密码进行查询
        Employee one = employeeService.getOne(wrapper);

        //3.如果没有查到数据代表用户名或密码错误
        if (one == null) {
            return R.error("用户名或密码错误!");
        }

        //4.如果查到了,查看用户的状态是不是禁用状态
        if (one.getStatus() != 1) {
            return R.error("用户已被禁用!");
        }

        //5.如果status==1代表用户登录成功

        session.setAttribute("id", one.getId());

        return R.success(one);

    }

    /**
     * 退出功能
     * 前端发送请求:
     * url:http://localhost:8080/employee/logout
     * mehotd:post
     * 前端需要的
     * code:1取消操作成功
     */
    @PostMapping("logout")
    public R logout(HttpSession session) {
//        A. 清理Session中的用户id
        session.removeAttribute("id");
//        B. 返回结果
        return R.success(null);
    }

    /**
     * 新增员工
     * @param employee
     * @return
     */
    @PostMapping
    public R<String> save(HttpServletRequest request,@RequestBody Employee employee){
        log.info("新增员工，员工信息：{}",employee.toString());

        //设置初始密码123456，需要进行md5加密处理
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
        
        employee.setCreateTime(LocalDateTime.now());
        employee.setUpdateTime(LocalDateTime.now());
        
        //获得当前登录用户的id
        Long empId = (Long) request.getSession().getAttribute("id");

        employee.setCreateUser(empId);
        employee.setUpdateUser(empId);

        System.out.println("---> employee = " + employee.toString());
        try{
            employeeService.save(employee);
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("新增员工失败");
        }
        return R.success("新增员工成功");
    }
}
