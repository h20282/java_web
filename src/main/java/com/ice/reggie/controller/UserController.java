package com.ice.reggie.controller;

import com.ice.reggie.pojo.Employee;
import com.ice.reggie.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
   @description 
   @author Milo
   @date 2022/1/5 10:08
*/
@RequestMapping("user")
@RestController
public class UserController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public String sayHello(){
        return "hello springboot";
    }

    //localhost:8080/user/1      Delete  删除
    //localhost:8080/user/1      Get     查询
    //                           Post    新增
    //                           Put     修改

    @GetMapping("{id}")
    public Employee findById(@PathVariable Integer id){
      return   employeeService.getById(id);

    }



}
