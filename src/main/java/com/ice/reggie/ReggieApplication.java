package com.ice.reggie;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
   @description 
   @author Milo
   @date 2022/1/5 10:01
*/
@SpringBootApplication
@MapperScan("com.ice.reggie.mapper")
@Slf4j
@ServletComponentScan
public class ReggieApplication {
    public static void main(String[] args) {
        SpringApplication.run(ReggieApplication.class,args);
        log.info("项目启动...");
//        System.out.println("项目启动...");
    }
}

/**
 * springboot功能
 * 1.起步依赖
 *  基于Maven的依赖传递
 *  使用springboot项目需要使用某个技术,只需要导入该技术的起步依赖,其他的依赖会通过依赖传递的方式导入
 *  springboot内置了非常多的版本
 * 2.自动配置
 *  作用:使用某一项技术不需要复杂的配置,springboot已经帮你配置好了
 *  springboot自动配置原理?
 *  a.启动引导类上配置了SpringBootApplication注解
 *  b.该注解中又配置@EnableAutoConfiguration开启自动配置
 *  c.开启自动配置注解中使用了@Import({AutoConfigurationImportSelector.class})
 *  d.spring会将AutoConfigurationImportSelector类中的selectImports方法的返回值创建bean对象存入spring容器
 *  e.selectImports方法的返回值是从META-INF/spring.factories中读取出来的
 *  f.并不是所有的配置类都会创建对象,需要满足@Conditional条件的才会创建对象
 *  g.如果你导入了某个技术的依赖,或者配置了某个技术的参数,那么才能满足条件,创建该技术的自动配置类的对象,完成自动配置
 *
 * 3.其他辅助功能
 *
 *
 *
 * mybatisplus
 * 1.简化mybatis开发,单表增删改查都不需要手动写sql
 * 2.使用
 *      a.写一个实体类
 *      b.导入配置
 *      c.写一个Mapper接口继承BaseMapper<表对应的实体类>
 *      d.写一个service接口继承IService<表对应的实体类>接口
 *      e.写一个service实现类,继承ServiceImpl<映射接口.class,表对应的实体类> 实现自己写的接口
 *      f.在Service实现类上加上@Service注解
 *      g.在Controller中直接使用Autowired注入Service即可使用
 *      h.可以使用LambdaQueryWrapper来构建条件
 *
 */
